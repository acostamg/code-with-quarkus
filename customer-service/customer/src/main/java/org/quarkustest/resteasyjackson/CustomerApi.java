package org.quarkustest.resteasyjackson;

import org.quarkustest.entity.Customer;
import org.quarkustest.repository.CustomerRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/customer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerApi {

    @Inject
    CustomerRepository pr;

    @GET
    public List<Customer> list() {
        return pr.listCustomer();
    }

    @GET
    @Path("/{id}")
    public Customer getById(@QueryParam("id") Long id) {
        return pr.findCustomer(id);
    }

    @POST
    public Response add(Customer c) {
        c.getProducts().forEach(p-> p.setCustomer(c));
        pr.createdCustomer(c);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@QueryParam("id") Long id) {
        Customer customer = pr.findCustomer(id);
        pr.deleteCustomer(customer);
        return Response.ok().build();
    }
    @PUT
    public Response update(Customer p) {
        Customer customer = pr.findCustomer(p.getId());
        customer.setCode(p.getCode());
        customer.setAccountNumber(p.getAccountNumber());
        customer.setSurname(p.getSurname());
        customer.setPhone(p.getPhone());
        customer.setAddress(p.getAddress());
        customer.setProducts(p.getProducts());
        pr.updateCustomer(customer);
        return Response.ok().build();
    }

}
