package org.quarkustest;

import io.quarkus.test.junit.NativeImageTest;

@NativeImageTest
public class NativeMyObjectMapperCustomizerIT extends MyObjectMapperCustomizerTest {

    // Execute the same tests but in native mode.
}