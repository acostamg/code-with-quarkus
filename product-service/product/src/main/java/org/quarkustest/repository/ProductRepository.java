package org.quarkustest.repository;

import org.quarkustest.entity.Product;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class ProductRepository {

    @Inject
    EntityManager entityManager;

    @Transactional
    public void createProduct(Product product) {
        entityManager.persist(product);
    }

    @Transactional
    public void deleteProduct(Product product) {
        entityManager.remove(entityManager.contains(product) ? product : entityManager.merge(product));
    }

    @Transactional
    public List<Product> getAllProducts() {
        List<Product> products = entityManager.createQuery("select p from Product p").getResultList();
        return products;
    }

     @Transactional
     public Product getProductById(Long id) {
        return entityManager.find(Product.class, id);
    }

    @Transactional
    public void updateProduct(Product product) {
        entityManager.merge(product);
    }


}
