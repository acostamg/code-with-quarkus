package org.quarkustest.resteasyjackson;


import lombok.Data;
import org.quarkustest.entity.Product;
import org.quarkustest.repository.ProductRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Data
@Path("/product")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductApi {

    @Inject
    ProductRepository productRepository;

    @GET
    public List<Product> listAll() {
        return productRepository.getAllProducts();
    }

    @POST
    public Response add(Product product) {
        productRepository.createProduct(product);
        return Response.ok().build();
    }

    @GET
    @Path("/{id}")
    public Product getProductById(@QueryParam("Id") Long id) {
        var product = productRepository.getProductById(id);
        return Optional.ofNullable(product)
                .orElseThrow(
                        () -> new NotFoundException(String.format("Product with id {} does not exists", id)));
    }


    @DELETE
    public Response delete(Product product) {
        productRepository.deleteProduct(product);
        return Response.ok().build();
    }

    @PUT
    public Response update(Product product) {
        productRepository.updateProduct(product);
        return Response.ok().build();
    }


}
